import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:insta_currency/core/constants/app_routes.dart';
import 'package:insta_currency/core/constants/app_themes.dart';
import 'package:insta_currency/features/language/data/app_languages.dart';
import 'package:insta_currency/core/helper/app_localizations.dart';
import 'package:insta_currency/core/helper/route_generator.dart';
import 'package:insta_currency/features/language/presenter/bloc/lang_bloc.dart';
import 'package:insta_currency/features/language/presenter/bloc/lang_state.dart';

import 'core/widgets/screen_util_init.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ScreenInit(
      builder: (BuildContext context, Widget? widget) {
        RouteGenerator generator = RouteGenerator();
        return BlocBuilder<LanguageCubit, LanguageState>(
            builder: (BuildContext context, LanguageState state) {
          return MaterialApp(
            debugShowCheckedModeBanner: false,
            locale: Locale(LanguageCubit.lang),
            supportedLocales: AppLanguages.values.map((e) => Locale(e.value)),
            localizationsDelegates: const <LocalizationsDelegate<dynamic>>[
              AppLocalizations.delegate,
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
              GlobalCupertinoLocalizations.delegate,
              DefaultCupertinoLocalizations.delegate
            ],
            onGenerateRoute: generator.generateRoute,
            initialRoute: AppRoute.splashScreen,
            theme: Themes.light(context),
          );
        });
      },
    );
  }
}
