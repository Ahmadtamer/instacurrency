import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';

@Injectable()
class ApiService {
  late Dio _dio;
  final baseUrl = 'https://api.exchangerate.host/';

  ApiService() {
    BaseOptions options = BaseOptions(
      baseUrl: baseUrl,
      receiveDataWhenStatusError: true,
      connectTimeout: 20 * 1000,
      // 60 seconds,
      receiveTimeout: 20 * 1000,
    );

    _dio = Dio(options);
  }

  Future<Response<T>> postApi<T>(
    String path, {
    Map<String, dynamic> body = const {},
  }) async =>
      await _dio.post(path, data: body);

  Future<Response<T>> getApi<T>(
    String path, {
    Map<String, dynamic> queryParameters = const {},
  }) async =>
      _dio.get(path, queryParameters: queryParameters);

  Future<Response<T>> putApi<T>(
    String path, {
    Map<String, dynamic> body = const {},
  }) async =>
      await _dio.put(path, data: body);
}
