import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:insta_currency/features/currency_exchange/presenter/bloc/convert_currency_bloc/convert_currency_bloc.dart';
import 'package:insta_currency/features/historical_rates/presenter/bloc/get_currency_rates_bloc/convert_currency_bloc.dart';
import 'package:insta_currency/features/history/presenter/bloc/get_history_bloc/get_history_bloc.dart';
import 'package:insta_currency/features/language/presenter/bloc/lang_bloc.dart';

import 'dependencies/dependency_init.dart';

class AppMainBlocProvider extends StatelessWidget {
  AppMainBlocProvider({Key? key, this.child}) : super(key: key);

  final LanguageCubit _languageCubit = getIt<LanguageCubit>();
  final ConvertCurrencyCubit _convertCurrencyCubit =
      getIt<ConvertCurrencyCubit>();
  final GetHistoryCubit _getHistoryCubit = getIt<GetHistoryCubit>();
  final GetCurrencyRatesCubit _currencyRatesCubit =
      getIt<GetCurrencyRatesCubit>();
  final Widget? child;

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: <BlocProvider<dynamic>>[
        BlocProvider<LanguageCubit>(
            create: (BuildContext context) => _languageCubit..getLanguage()),
        BlocProvider<ConvertCurrencyCubit>(
            create: (BuildContext context) => _convertCurrencyCubit),
        BlocProvider<GetHistoryCubit>(
            create: (BuildContext context) => _getHistoryCubit),
        BlocProvider<GetCurrencyRatesCubit>(
            create: (BuildContext context) => _currencyRatesCubit),
      ],
      child: child!,
    );
  }
}
