abstract class AppImages {
  static const String _base = "assets/";
  static const String _images = "${_base}images/";
  static const String _icons = "${_base}icons/";

  // images
  static const String logo = '${_images}app_icon.png';
  static const noInternetImg = '${_images}no_internet.png';

  // icons
  static const String exchange = '${_icons}exchange.svg';
  static const String history = '${_icons}history.svg';
  static const String chart = '${_icons}chart.svg';
}
