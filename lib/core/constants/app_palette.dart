import 'package:flutter/material.dart';

enum PaletteType { light, dark }

class AppPalette {
  static const Color primaryColor = Color(0xff46658C);
  static const Color black = Colors.black;
  static const Color white = Colors.white;

  static const Color lightBlue = Color(0xffe8f4f8);
  static const Color primaryBackground = Color(0xffF1F2E7);
  static const Color primaryGreen = Color(0xffA3BF8F);
  static const Color rose = Color(0xffF272A1);
  static const Color lightRose = Color(0xffF2A29B);
  static const Color red = Color(0xffD9435F);
  static const Color brown = Color(0xff783D3E);
  static const Color darkBrown = Color(0xff473D32);
  static const Color lightBrown = Color(0xffD9D3C1);
  static const Color darkGrey = Color(0xff343A40);
  static const Color grey = Color(0xff9BB9C3);
  static const Color defaultGrey = Colors.grey;
  static const Color lightGrey = Color(0xffFAFAFA);

}
