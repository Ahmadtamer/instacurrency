class AppRoute {
  static const String splashScreen = '/';
  static const String homeScreen = '/homeScreen';
  static const String historicalRatesScreen = '/historicalRatesScreen';
  static const String currencyExchangeScreen = '/currencyExchangeScreen';
}
