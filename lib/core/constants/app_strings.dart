abstract class AppStrings {
  static const String appName = "appName";
  static const String required = "required";
  static const String english = "english";
  static const String arabic = "arabic";
  static const String currencyExchange = "currencyExchange";
  static const String history = "history";
  static const String rates = "rates";
  static const String convert = "convert";
  static const String save = "save";
  static const String from = "from";
  static const String to = "to";
  static const String changes = "changes";
  static const String dates = "dates";
}
