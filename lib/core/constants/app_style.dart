import 'package:flutter/material.dart';
import 'package:insta_currency/core/constants/app_palette.dart';

class AppStyle {
  static const smallHorizontalPadding = EdgeInsets.symmetric(horizontal: 12);
  static const largeHorizontalPadding = EdgeInsets.symmetric(horizontal: 32);
  static const largeHorizontalSpace = EdgeInsets.symmetric(horizontal: 64);
  static const smallVerticalPadding = EdgeInsets.symmetric(vertical: 8);
  static const midVerticalPadding = EdgeInsets.symmetric(vertical: 12);

  static const largePadding =
      EdgeInsets.symmetric(horizontal: 32, vertical: 8);

  static BoxDecoration littleRoundedContainer = BoxDecoration(
      color: AppPalette.grey.withOpacity(0.3),
      borderRadius: const BorderRadius.all(Radius.circular(20)));

  static BoxDecoration roundedContainer = BoxDecoration(
      color: AppPalette.grey.withOpacity(0.1),
      borderRadius: const BorderRadius.all(Radius.circular(30)));

  static const BoxDecoration roundedBottomContainer = BoxDecoration(
    color: AppPalette.lightBlue,
    borderRadius: BorderRadius.only(
      topRight: Radius.circular(40.0),
      topLeft: Radius.circular(40.0),
    ),
  );
}
