import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'app_palette.dart';

class Fonts {
  static const String roboto = "Roboto";
}

class AppTextStyles {
  BuildContext context;

  AppTextStyles(this.context);

  TextStyle get h1 => TextStyle(
        fontWeight: FontWeight.w500,
        letterSpacing: 1.0,
        fontSize: 40.sp,
        fontFamily: Fonts.roboto,
      );

  TextStyle get h2 => TextStyle(
        fontWeight: FontWeight.w500,
        letterSpacing: 0.5,
        fontSize: 20.sp,
        fontFamily: Fonts.roboto,
        color: AppPalette.primaryColor, // default to primaryColor
      );

  TextStyle get h2Bold => TextStyle(
        fontWeight: FontWeight.bold,
        letterSpacing: 0.5,
        fontSize: 20.sp,
        fontFamily: Fonts.roboto,
        color: AppPalette.primaryColor, // default to primaryColor
      );

  TextStyle get h3 => TextStyle(
        fontWeight: FontWeight.w500,
        letterSpacing: 0.5,
        fontSize: 16.sp,
        height: 21 / 16,
        fontFamily: Fonts.roboto,
        color: Theme.of(context)
            .accentColor
            .withOpacity(0.9), // default to accentColor
      );

  TextStyle get body => TextStyle(
        fontWeight: FontWeight.w400,
        letterSpacing: 0.5,
        fontSize: 16.sp,
        height: 21 / 16,
        fontFamily: Fonts.roboto,
        color: Theme.of(context).accentColor, // default to accentColor
      );

  TextStyle get small => TextStyle(
        fontWeight: FontWeight.w500,
        letterSpacing: 0.4,
        fontSize: 12.sp,
        fontFamily: Fonts.roboto,
        height: 21 / 16,
      );

  TextStyle get cpCode => TextStyle(
        fontWeight: FontWeight.w500,
        fontSize: 26.sp,
        letterSpacing: 0.5,
        fontFamily: Fonts.roboto,
        color: AppPalette.black,
      );
}
