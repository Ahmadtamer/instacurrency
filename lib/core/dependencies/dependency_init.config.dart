// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;

import '../../features/currency_exchange/domain/repo/convert_currency_repo.dart'
    as _i4;
import '../../features/currency_exchange/presenter/bloc/convert_currency_bloc/convert_currency_bloc.dart'
    as _i10;
import '../../features/historical_rates/domain/repo/currency_rates_repo.dart'
    as _i5;
import '../../features/historical_rates/presenter/bloc/get_currency_rates_bloc/convert_currency_bloc.dart'
    as _i7;
import '../../features/history/domain/repo/history_repo.dart' as _i8;
import '../../features/history/presenter/bloc/get_history_bloc/get_history_bloc.dart'
    as _i11;
import '../../features/language/domain/local_repository/lang_reposirory.dart'
    as _i9;
import '../../features/language/presenter/bloc/lang_bloc.dart' as _i12;
import '../apis/api_service.dart' as _i3;
import '../helper/database_hepler.dart'
    as _i6; // ignore_for_file: unnecessary_lambdas

// ignore_for_file: lines_longer_than_80_chars
/// initializes the registration of provided dependencies inside of [GetIt]
_i1.GetIt $initGetIt(
  _i1.GetIt get, {
  String? environment,
  _i2.EnvironmentFilter? environmentFilter,
}) {
  final gh = _i2.GetItHelper(
    get,
    environment,
    environmentFilter,
  );
  gh.factory<_i3.ApiService>(() => _i3.ApiService());
  gh.factory<_i4.ConvertCurrencyRepo>(
      () => _i4.ConvertCurrencyRepo(get<_i3.ApiService>()));
  gh.factory<_i5.CurrencyRatesRepo>(
      () => _i5.CurrencyRatesRepo(get<_i3.ApiService>()));
  gh.factory<_i6.DatabaseHelper>(() => _i6.DatabaseHelper());
  gh.factory<_i7.GetCurrencyRatesCubit>(
      () => _i7.GetCurrencyRatesCubit(get<_i5.CurrencyRatesRepo>()));
  gh.factory<_i8.HistoryRepo>(() => _i8.HistoryRepo(get<_i6.DatabaseHelper>()));
  gh.factory<_i9.LanguageRepository>(() => _i9.LanguageRepository());
  gh.factory<_i10.ConvertCurrencyCubit>(() => _i10.ConvertCurrencyCubit(
        get<_i4.ConvertCurrencyRepo>(),
        get<_i6.DatabaseHelper>(),
      ));
  gh.factory<_i11.GetHistoryCubit>(
      () => _i11.GetHistoryCubit(get<_i8.HistoryRepo>()));
  gh.factory<_i12.LanguageCubit>(
      () => _i12.LanguageCubit(get<_i9.LanguageRepository>()));
  return get;
}
