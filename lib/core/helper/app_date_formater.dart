import 'package:intl/intl.dart';
import 'package:jiffy/jiffy.dart';

class AppDateFormatter {
  static String calculateTimeDifferenceFromNow(String dateTime) => Jiffy(
          DateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(dateTime, true),
          "yyyy-MM-dd'T'HH:mm:ss.SSS'Z")
      .fromNow();

  static List<DateTime> getWeekForNow() {
    List<DateTime> week = [];
    for (int i = 7; i >= 0; i--) {
      week.add(DateTime.now().subtract(Duration(days: i)));
    }
    return week;
  }
}
