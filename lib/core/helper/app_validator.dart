import 'package:flutter/cupertino.dart';
import 'package:insta_currency/core/constants/app_strings.dart';
import 'package:insta_currency/core/helper/app_localizations.dart';

class AppValidator {
  static String? validatorRequired(String? value, BuildContext context) {
    if (value == null || value.isEmpty) {
      return AppLocalizations.of(context)?.translate(AppStrings.required);
    }
    return null;
  }
}
