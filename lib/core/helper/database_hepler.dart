import 'dart:async';
import 'dart:io' as io;

import 'package:flutter/foundation.dart';
import 'package:injectable/injectable.dart';
import 'package:insta_currency/features/history/data/model/converted_currencies.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

@Injectable()
class DatabaseHelper {
  static final DatabaseHelper _instance = DatabaseHelper.internal();

  factory DatabaseHelper() => _instance;

  static Database? _db;

  Future<Database> get db async {
    if (_db != null) return _db!;
    _db = await initDb();
    return _db!;
  }

  DatabaseHelper.internal();

  initDb() async {
    io.Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "main.db");
    var theDb = await openDatabase(path, version: 1, onCreate: _onCreate);
    return theDb;
  }

  void _onCreate(Database db, int version) async {
    // When creating the db, create the table
    await db.execute(
        "CREATE TABLE ConvertedCurrencies(id INTEGER PRIMARY KEY AUTOINCREMENT, fromCurrency TEXT, toCurrency TEXT, amount REAL, result REAL)");
  }

  Future<int> addConvertedCurrencies(
      ConvertedCurrencies convertedCurrencies) async {
    var dbClient = await db;
    int res = await dbClient.insert(
        "ConvertedCurrencies", convertedCurrencies.toJson());
    return res;
  }

  Future<List<ConvertedCurrencies>> getAllConvertedCurrencies() async {
    var dbClient = await db;
    List<Map> list =
        await dbClient.rawQuery('SELECT * FROM ConvertedCurrencies');
    List<ConvertedCurrencies> convertedCurrencyList = [];
    for (int i = 0; i < list.length; i++) {
      var item = ConvertedCurrencies.fromJson(list[i]);
      convertedCurrencyList.add(item);
    }
    if (kDebugMode) {
      print(convertedCurrencyList.length);
    }
    return convertedCurrencyList;
  }
}
