import 'package:flutter/material.dart';
import 'package:insta_currency/core/constants/app_palette.dart';
import 'package:insta_currency/core/constants/app_routes.dart';
import 'package:insta_currency/core/screens/home_screen.dart';
import 'package:insta_currency/features/currency_exchange/presenter/screen/currency_exchange_screen.dart';
import 'package:insta_currency/core/screens/splash_screen.dart';
import 'package:insta_currency/features/historical_rates/presenter/screen/historical_rates_screen.dart';

class RouteGenerator {
  // late DatabaseHelper db;
  //
  // RouteGenerator() {
  //   db = DatabaseHelper();
  // }

  Route<dynamic> generateRoute(RouteSettings settings) {
    // Getting arguments passed in while calling Navigator.pushNamed
    switch (settings.name) {
      case AppRoute.splashScreen:
        return _screenInit(const SplashScreen(), settings);

      case AppRoute.homeScreen:
        return _screenInit(const HomeScreen(), settings);

      case AppRoute.historicalRatesScreen:
        return _screenInit(const HistoricalRatesScreen(), settings);

      case AppRoute.currencyExchangeScreen:
        return _screenInit(const CurrencyExchangeScreen(), settings);

      default:
        return _errorRoute();
    }
  }

  static MaterialPageRoute<dynamic> _screenInit(
      Widget screen, RouteSettings settings) {
    return MaterialPageRoute<dynamic>(
        builder: (_) => screen, settings: settings);
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute<dynamic>(builder: (_) {
      return Scaffold(
        appBar: AppBar(
          backgroundColor: AppPalette.primaryColor,
          title: const Text('Error'),
        ),
        body: const Center(
          child: Text('ERROR'),
        ),
      );
    });
  }
}
