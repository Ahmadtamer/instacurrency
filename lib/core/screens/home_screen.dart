import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:insta_currency/core/constants/app_image_paths.dart';
import 'package:insta_currency/core/constants/app_strings.dart';
import 'package:insta_currency/core/widgets/bottom_bar_widget.dart';
import 'package:insta_currency/features/currency_exchange/presenter/screen/currency_exchange_screen.dart';
import 'package:insta_currency/features/historical_rates/presenter/screen/historical_rates_screen.dart';
import 'package:insta_currency/features/history/presenter/screen/history_screen.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int _selectedPageIndex = 0;

  List<Widget> screens = [
    const HistoricalRatesScreen(),
    const CurrencyExchangeScreen(),
    const HistoryScreen()
  ];

  List<String> titles = [
    AppStrings.rates,
    AppStrings.currencyExchange,
    AppStrings.history
  ];

  List<String> iconPaths = [
    AppImages.chart,
    AppImages.exchange,
    AppImages.history,
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(),
      body: screens[_selectedPageIndex],
      bottomNavigationBar: BottomBarWidget(
        screenIndex: (value) => setState(() => _selectedPageIndex = value),
        titles: titles,
        iconsPath: iconPaths,
      ),
    );
  }

  AppBar buildAppBar() {
    return AppBar(
      title: Image.asset(AppImages.logo, height: 130.h),
      centerTitle: true,
      toolbarHeight: 110.h,
    );
  }
}
