import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:insta_currency/core/constants/app_image_paths.dart';
import 'package:insta_currency/core/constants/app_palette.dart';
import 'package:insta_currency/core/constants/app_routes.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  late Timer timer;

  @override
  void initState() {
    super.initState();
    timer = Timer.periodic(const Duration(seconds: 3), (timer) async {
      Navigator.pushReplacementNamed(context, AppRoute.homeScreen);
    });
  }

  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppPalette.primaryColor,
      body: Center(
        child: Padding(
          padding: EdgeInsets.all(64.w),
          child: Image.asset(AppImages.logo),
        ),
      ),
    );
  }
}
