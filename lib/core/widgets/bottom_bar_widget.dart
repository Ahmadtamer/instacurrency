import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_snake_navigationbar/flutter_snake_navigationbar.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:insta_currency/core/constants/app_palette.dart';
import 'package:insta_currency/core/helper/app_localizations.dart';

class BottomBarWidget extends StatefulWidget {
  final List<String> titles;
  final List<String> iconsPath;
  final ValueChanged screenIndex;

  const BottomBarWidget(
      {Key? key,
      required this.titles,
      required this.iconsPath,
      required this.screenIndex})
      : super(key: key);

  @override
  State<BottomBarWidget> createState() => _BottomBarWidgetState();
}

class _BottomBarWidgetState extends State<BottomBarWidget> {
  SnakeBarBehaviour snakeBarStyle = SnakeBarBehaviour.floating;
  SnakeShape snakeShape = SnakeShape.indicator;
  Color selectedColor = AppPalette.primaryColor;
  Color unselectedColor = AppPalette.defaultGrey;
  bool showSelectedLabels = true;
  bool showUnselectedLabels = true;
  int _selectedPageIndex = 0;

  @override
  Widget build(BuildContext context) {
    return SnakeNavigationBar.color(
      behaviour: snakeBarStyle,
      snakeShape: snakeShape,
      backgroundColor: AppPalette.lightGrey,
      snakeViewColor: selectedColor,
      selectedItemColor:
          snakeShape == SnakeShape.indicator ? selectedColor : null,
      unselectedItemColor: unselectedColor,
      showUnselectedLabels: showUnselectedLabels,
      showSelectedLabels: showSelectedLabels,
      currentIndex: _selectedPageIndex,
      onTap: (index) {
        setState(() => _selectedPageIndex = index);
        widget.screenIndex(index);
      },
      items: [
        for (int i = 0; i < widget.titles.length; i++)
          BottomNavigationBarItem(
            label: AppLocalizations.of(context)?.translate(widget.titles[i]),
            icon: SvgPicture.asset(widget.iconsPath[i],
                color:
                    _selectedPageIndex == i ? selectedColor : unselectedColor,
                height: 16.h),
          ),
      ],
      selectedLabelStyle: const TextStyle(fontSize: 12),
      unselectedLabelStyle: const TextStyle(fontSize: 8),
    );
  }
}
