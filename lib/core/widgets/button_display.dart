import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:insta_currency/core/constants/app_palette.dart';

import 'text_display.dart';

class AppButton extends StatelessWidget {
  final String translation;
  final Color color;
  final Color textColor;
  final Color borderColor;
  final double fontSize;
  final FontWeight fontWeight;
  final String fontFamily;
  final TextDecoration? textDecoration;
  final ButtonStyle? style;
  final void Function()? onTap;
  RoundedRectangleBorder? border;
  final EdgeInsets padding;

  AppButton(
      {super.key,
      required this.translation,
      this.color = AppPalette.primaryColor,
      this.textColor = AppPalette.white,
      this.borderColor = AppPalette.primaryColor,
      this.onTap,
      this.fontSize = 24,
      this.fontWeight = FontWeight.w600,
      this.fontFamily = 'Roboto',
      this.textDecoration,
      this.border,
      this.padding = const EdgeInsets.symmetric(vertical: 24, horizontal: 12),
      this.style});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 0.8.sw,
      height: 60.h,
      child: ElevatedButton(
          style: style ??
              ElevatedButton.styleFrom(
                backgroundColor: color,
                shape: border ??
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15),
                      side: BorderSide(color: borderColor),
                    ),
              ),
          onPressed: onTap,
          child: AppTextDisplay(
            translation: translation,
            color: textColor,
            fontSize: fontSize,
            fontWeight: fontWeight,
            fontFamily: fontFamily,
            decoration: textDecoration,
          )),
    );
  }
}
