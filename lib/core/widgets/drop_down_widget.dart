import 'package:flutter/material.dart';
import 'package:insta_currency/core/constants/app_palette.dart';

class DropDownAppWidget extends StatefulWidget {
  final List<String> dropDownList;
  String? chosenValue;
  final ValueChanged<String?> selected;

  DropDownAppWidget({
    super.key,
    required this.selected,
    required this.dropDownList,
    required this.chosenValue,
  }); // Fix this line.

  @override
  _DropDownAppWidgetState createState() => _DropDownAppWidgetState();
}

class _DropDownAppWidgetState extends State<DropDownAppWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 5.0),
      decoration: BoxDecoration(
          border: Border.all(color: AppPalette.primaryColor),
          borderRadius: BorderRadius.circular(20.0)),
      child: DropdownButton<String>(
        value: widget.chosenValue,
        icon: const Icon(Icons.arrow_drop_down),
        iconSize: 24,
        elevation: 16,
        style: const TextStyle(color: AppPalette.primaryColor),
        underline: Container(
          height: 2,
          color: Colors.transparent,
        ),
        onChanged: (String? newValue) {
          setState(() {
            widget.chosenValue = newValue;
            widget.selected(newValue);
          });
        },
        items:
            widget.dropDownList.map<DropdownMenuItem<String>>((String value) {
          return DropdownMenuItem<String>(
            value: value,
            child: Text(value),
          );
        }).toList(),
      ),
    );
  }
}
