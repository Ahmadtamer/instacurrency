import 'package:flutter/material.dart';
import 'package:insta_currency/core/constants/app_palette.dart';

class LoadingWidget extends StatelessWidget {
  const LoadingWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => const Center(
      child: CircularProgressIndicator(color: AppPalette.primaryColor));
}
