import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:insta_currency/core/helper/app_localizations.dart';

class AppTextDisplay extends StatefulWidget {
  final Color? color;
  final double fontSize;
  final String? text;
  final String? translation;
  final List? translationValues;
  final FontWeight fontWeight;
  final String fontFamily;
  TextStyle? style;
  final TextAlign textAlign;
  final bool isUpper;
  final bool softWrap;
  final bool underLine;
  final int maxLines;
  final TextOverflow overflow;
  final TextDecoration? decoration;

  AppTextDisplay(
      {super.key,
      this.color,
      this.fontSize = 16.0,
      this.text,
      this.fontFamily = 'Roboto',
      this.decoration,
      this.translation,
      this.translationValues,
      this.overflow = TextOverflow.ellipsis,
      this.style,
      this.softWrap = false,
      this.maxLines = 2,
      this.textAlign = TextAlign.center,
      this.fontWeight = FontWeight.normal,
      this.isUpper = false,
      this.underLine = false});

  @override
  State<AppTextDisplay> createState() => _AppTextDisplayState();
}

class _AppTextDisplayState extends State<AppTextDisplay> {
  @override
  Widget build(BuildContext context) {
    if (widget.style != null) {
      double fontSize = widget.style!.fontSize ?? 16;
      widget.style = widget.style!.copyWith(fontSize: fontSize.sp);
    }
    return Text(
      widget.translation != null
          ? AppLocalizations.of(context)?.translate(widget.translation!) ?? ""
          : widget.text != null
              ? widget.text!
              : "",
      textAlign: widget.textAlign,
      overflow: widget.overflow,
      maxLines: widget.maxLines,
      softWrap: widget.softWrap,
      style: widget.style ??
          TextStyle(
              decoration: widget.underLine
                  ? TextDecoration.underline
                  : widget.decoration,
              color: widget.color ?? Theme.of(context).accentColor,
              fontSize: widget.fontSize,
              fontFamily: widget.fontFamily,
              fontWeight: widget.fontWeight),
    );
  }
}
