class CurrencyConvertedModel {
  bool? success;
  Query? query;
  Info? info;
  bool? historical;
  String? date;
  double? result;

  CurrencyConvertedModel({
    this.success,
    this.query,
    this.info,
    this.historical,
    this.date,
    this.result,
  });

  CurrencyConvertedModel.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    query = json['query'] != null ? Query.fromJson(json['query']) : null;
    info = json['info'] != null ? Info.fromJson(json['info']) : null;
    historical = json['historical'];
    date = json['date'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['success'] = success;
    if (query != null) {
      data['query'] = query!.toJson();
    }
    if (info != null) {
      data['info'] = info!.toJson();
    }
    data['historical'] = historical;
    data['date'] = date;
    data['result'] = result;
    return data;
  }
}

class Motd {
  String? msg;
  String? url;

  Motd({this.msg, this.url});

  Motd.fromJson(Map<String, dynamic> json) {
    msg = json['msg'];
    url = json['url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['msg'] = msg;
    data['url'] = url;
    return data;
  }
}

class Query {
  String? from;
  String? to;
  int? amount;

  Query({this.from, this.to, this.amount});

  Query.fromJson(Map<String, dynamic> json) {
    from = json['from'];
    to = json['to'];
    amount = json['amount'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['from'] = from;
    data['to'] = to;
    data['amount'] = amount;
    return data;
  }
}

class Info {
  double? rate;

  Info({this.rate});

  Info.fromJson(Map<String, dynamic> json) {
    rate = json['rate'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['rate'] = rate;
    return data;
  }
}
