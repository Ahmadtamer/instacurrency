import 'package:injectable/injectable.dart';
import 'package:insta_currency/core/apis/api_paths.dart';
import 'package:insta_currency/core/apis/api_service.dart';
import 'package:insta_currency/features/currency_exchange/data/model/currency_converted_model.dart';

@Injectable()
class ConvertCurrencyRepo {
  final ApiService apiService;

  ConvertCurrencyRepo(this.apiService);

  Future<CurrencyConvertedModel> convert(
      String from, String to, int amount) async {
    var response = await apiService.getApi(
      ApiPaths.convert,
      queryParameters: {
        "from": from,
        "to": to,
        "amount": amount,
      },
    );
    return CurrencyConvertedModel.fromJson(response.data);
  }
}
