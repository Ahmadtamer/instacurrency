import 'package:currency_picker/currency_picker.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:insta_currency/core/helper/database_hepler.dart';
import 'package:insta_currency/features/currency_exchange/data/model/currency_converted_model.dart';
import 'package:insta_currency/features/currency_exchange/domain/repo/convert_currency_repo.dart';
import 'package:insta_currency/features/currency_exchange/presenter/bloc/convert_currency_bloc/convert_currency_state.dart';
import 'package:insta_currency/features/history/data/model/converted_currencies.dart';

@Injectable()
class ConvertCurrencyCubit extends Cubit<ConvertCurrencyState> {
  final ConvertCurrencyRepo convertCurrencyRepo;
  final DatabaseHelper db;

  ConvertCurrencyCubit(this.convertCurrencyRepo, this.db)
      : super(ConvertCurrencyInitial());

  Future<void> convert(String from, String to, int amount) async {
    try {
      CurrencyConvertedModel response =
          await convertCurrencyRepo.convert(from, to, amount);
      if (response.success ?? false) {
        emit(ConvertCurrencySuccess(response));
      } else {
        emit(ConvertCurrencyFailed("Error!, Please try again later"));
      }
    } catch (e) {
      if (kDebugMode) {
        print(e.toString());
      }
      emit(ConvertCurrencyFailed(e.toString()));
    }
  }

  Future<void> saveConvertedCurrencies(String fromCurrency, String toCurrency,
      double amount, double result) async {
    await db.addConvertedCurrencies(ConvertedCurrencies(
        fromCurrency: fromCurrency,
        toCurrency: toCurrency,
        amount: amount,
        result: result));
  }
}
