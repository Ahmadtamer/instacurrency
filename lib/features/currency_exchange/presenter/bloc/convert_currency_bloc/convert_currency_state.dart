import 'package:insta_currency/features/currency_exchange/data/model/currency_converted_model.dart';

abstract class ConvertCurrencyState {}

class ConvertCurrencyInitial extends ConvertCurrencyState {}

class ConvertCurrencySuccess extends ConvertCurrencyState {
  final CurrencyConvertedModel convertedModel;

  ConvertCurrencySuccess(this.convertedModel);
}

class ConvertCurrencyFailed extends ConvertCurrencyState {
  final String error;

  ConvertCurrencyFailed(this.error);
}
