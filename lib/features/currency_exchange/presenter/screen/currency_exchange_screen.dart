import 'package:currency_picker/currency_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:insta_currency/core/constants/app_palette.dart';
import 'package:insta_currency/core/constants/app_strings.dart';
import 'package:insta_currency/core/constants/app_style.dart';
import 'package:insta_currency/core/dependencies/dependency_init.dart';
import 'package:insta_currency/core/helper/app_validator.dart';
import 'package:insta_currency/core/helper/double_extension.dart';
import 'package:insta_currency/core/widgets/button_display.dart';
import 'package:insta_currency/core/widgets/text_display.dart';
import 'package:insta_currency/core/widgets/text_field_display.dart';
import 'package:insta_currency/features/currency_exchange/presenter/bloc/convert_currency_bloc/convert_currency_bloc.dart';
import 'package:insta_currency/features/currency_exchange/presenter/bloc/convert_currency_bloc/convert_currency_state.dart';
import 'package:insta_currency/features/currency_exchange/presenter/widget/currency_widget.dart';

enum CurrencyTye { sourceCurrency, destinationCurrency }

class CurrencyExchangeScreen extends StatefulWidget {
  const CurrencyExchangeScreen({Key? key}) : super(key: key);

  @override
  State<CurrencyExchangeScreen> createState() => _CurrencyExchangeScreenState();
}

class _CurrencyExchangeScreenState extends State<CurrencyExchangeScreen> {
  Currency? sourceCurrency;
  Currency? destinationCurrency;
  final CurrencyService _currencyService = CurrencyService();
  final TextEditingController textEditingController = TextEditingController();
  ConvertCurrencyCubit convertCurrencyCubit = getIt<ConvertCurrencyCubit>();
  var formKey = GlobalKey<FormState>();
  double result = 0;

  @override
  void initState() {
    super.initState();
    List<Currency> currencies = _currencyService.getAll();
    if (currencies.isNotEmpty) {
      sourceCurrency = currencies.first;
      destinationCurrency = currencies[1];
    }
    convertCurrency();
  }

  convertCurrency() {
    if (sourceCurrency != null && destinationCurrency != null) {
      int amount = textEditingController.text.isNotEmpty
          ? int.parse(textEditingController.text)
          : 1;

      convertCurrencyCubit.convert(
        sourceCurrency!.code,
        destinationCurrency!.code,
        amount,
      );
    }
  }

  onCurrencyClicked(CurrencyTye currencyTye) {
    showCurrencyPicker(
      context: context,
      showFlag: true,
      showSearchField: true,
      showCurrencyName: true,
      showCurrencyCode: true,
      onSelect: (Currency currency) {
        switch (currencyTye) {
          case CurrencyTye.sourceCurrency:
            setState(() => sourceCurrency = currency);
            break;
          case CurrencyTye.destinationCurrency:
            setState(() => destinationCurrency = currency);
            break;
        }
        convertCurrency();
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Form(
          key: formKey,
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                32.verticalSpace,
                buildBaseCurrency(),
                24.verticalSpace,
                buildSourceCurrency(),
                24.verticalSpace,
                buildDestinationCurrency(),
                32.verticalSpace,
                buildConvertBtn(),
                16.verticalSpace,
                buildSaveBtn(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget buildConvertBtn() {
    return AppButton(
      translation: AppStrings.convert,
      onTap: () {
        if (formKey.currentState?.validate() ?? false) {
          convertCurrency();
        }
      },
    );
  }

  Widget buildSaveBtn() {
    return AppButton(
      translation: AppStrings.save,
      onTap: () {
        convertCurrencyCubit.saveConvertedCurrencies(
            sourceCurrency?.code ?? "",
            destinationCurrency?.code ?? "",
            double.parse(textEditingController.text),
            result.toPrecision(3));
      },
    );
  }

  Widget buildBaseCurrency() {
    return BlocBuilder(
      bloc: convertCurrencyCubit,
      builder: (context, state) {
        double? amount;
        if (state is ConvertCurrencySuccess) {
          amount = state.convertedModel.info?.rate?.toPrecision(3);
        }
        return Container(
          width: 0.6.sw,
          padding: AppStyle.smallVerticalPadding,
          decoration: AppStyle.roundedContainer,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              64.horizontalSpace,
              buildExchangeIcon(),
              32.horizontalSpace,
              buildValue(amount),
            ],
          ),
        );
      },
    );
  }

  Widget buildValue(double? amount) {
    return AppTextDisplay(
      text:
          "1 ${sourceCurrency?.code} = ${amount ?? "..."} ${destinationCurrency?.code}",
      fontSize: 14,
      fontWeight: FontWeight.w600,
    );
  }

  Widget buildExchangeIcon() {
    return Container(
      padding: const EdgeInsets.all(8),
      decoration: const BoxDecoration(
          shape: BoxShape.circle, color: AppPalette.primaryColor),
      child: const Icon(
        Icons.currency_exchange,
        color: AppPalette.white,
        size: 18,
      ),
    );
  }

  Widget buildSourceCurrency() {
    return Container(
      margin: AppStyle.largeHorizontalPadding,
      decoration: AppStyle.littleRoundedContainer,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          32.horizontalSpace,
          InkWell(
            onTap: () => onCurrencyClicked(CurrencyTye.sourceCurrency),
            child: (sourceCurrency != null)
                ? CurrencyWidget(currency: sourceCurrency!)
                : Container(),
          ),
          Expanded(
            child: AppEditText(
              text: "1",
              textAlign: TextAlign.end,
              keyboardType: TextInputType.phone,
              maxLength: 5,
              controller: textEditingController,
              validator: (value) =>
                  AppValidator.validatorRequired(value, context),
              onSubmitted: (value) => convertCurrency(),
            ),
          ),
          32.horizontalSpace,
        ],
      ),
    );
  }

  Widget buildDestinationCurrency() {
    return Container(
      margin: AppStyle.largeHorizontalPadding,
      padding: AppStyle.midVerticalPadding,
      decoration: AppStyle.littleRoundedContainer,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          32.horizontalSpace,
          InkWell(
            onTap: () => onCurrencyClicked(CurrencyTye.destinationCurrency),
            child: (destinationCurrency != null)
                ? CurrencyWidget(currency: destinationCurrency!)
                : Container(),
          ),
          BlocBuilder(
              bloc: convertCurrencyCubit,
              builder: (context, state) {
                if (state is ConvertCurrencySuccess &&
                    state.convertedModel.result != null) {
                  result = state.convertedModel.result!.toPrecision(3);
                }
                return Expanded(
                  child: state is ConvertCurrencySuccess
                      ? AppTextDisplay(
                          text: "$result",
                          textAlign: TextAlign.end,
                          maxLines: 1,
                        )
                      : Container(),
                );
              }),
          32.horizontalSpace,
        ],
      ),
    );
  }
}
