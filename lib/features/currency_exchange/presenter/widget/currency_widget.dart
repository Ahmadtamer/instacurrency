import 'package:currency_picker/currency_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:insta_currency/core/constants/app_palette.dart';
import 'package:insta_currency/core/widgets/text_display.dart';
import 'package:currency_picker/src/extensions.dart';

class CurrencyWidget extends StatelessWidget {
  final Currency currency;

  const CurrencyWidget({
    Key? key,
    required this.currency,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        buildCurrencyIcon(),
        64.horizontalSpace,
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(children: [buildCurrencyCode(), buildArrowIcon()]),
            buildCurrencyName(),
          ],
        )
      ],
    );
  }

  Widget buildArrowIcon() {
    return const Icon(
      Icons.keyboard_arrow_down,
      color: AppPalette.primaryColor,
    );
  }

  Widget buildCurrencyCode() {
    return AppTextDisplay(
      text: currency.code,
      fontSize: 16,
      fontWeight: FontWeight.w600,
    );
  }

  Widget buildCurrencyName() {
    return AppTextDisplay(
      maxLines: 1,
      text: currency.name,
      fontSize: 12,
      textAlign: TextAlign.center,
    );
  }

  Widget buildCurrencyIcon() {
    return (currency.flag == null)
        ? Image.asset(
            'no_flag.png'.imagePath,
            package: 'currency_picker',
            width: 24,
          )
        : Container(
            padding: const EdgeInsets.all(8),
            decoration: const BoxDecoration(
                shape: BoxShape.circle, color: AppPalette.white),
            child: Text(CurrencyUtils.currencyToEmoji(currency)));
  }
}
