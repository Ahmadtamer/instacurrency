class CurrencyModel {
  double? startRate;
  double? endRate;
  double? change;
  double? changePct;

  CurrencyModel({this.startRate, this.endRate, this.change, this.changePct});

  CurrencyModel.fromJson(Map<String, dynamic> json) {
    startRate = json['start_rate'];
    endRate = json['end_rate'];
    change = json['change'];
    changePct = json['change_pct'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['start_rate'] = startRate;
    data['end_rate'] = endRate;
    data['change'] = change;
    data['change_pct'] = changePct;
    return data;
  }
}
