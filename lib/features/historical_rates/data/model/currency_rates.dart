import 'package:insta_currency/features/historical_rates/data/model/rates_model.dart';

class CurrencyRates {
  bool? success;
  bool? fluctuation;
  String? startDate;
  String? endDate;
  Rates? rates;

  CurrencyRates({
    this.success,
    this.fluctuation,
    this.startDate,
    this.endDate,
    this.rates,
  });

  CurrencyRates.fromJson(Map<String, dynamic> json, String currencyCode) {
    success = json['success'];
    fluctuation = json['fluctuation'];
    startDate = json['start_date'];
    endDate = json['end_date'];
    rates = json['rates'] != null
        ? Rates.fromJson(json['rates'], currencyCode)
        : null;
  }

  Map<String, dynamic> toJson(String currencyCode) {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['success'] = success;
    data['fluctuation'] = fluctuation;
    data['start_date'] = startDate;
    data['end_date'] = endDate;
    if (rates != null) {
      data['rates'] = rates!.toJson(currencyCode);
    }
    return data;
  }
}
