import 'package:insta_currency/features/historical_rates/data/model/currency_model.dart';

class Rates {
  CurrencyModel? currencyModel;

  Rates({this.currencyModel});

  Rates.fromJson(Map<String, dynamic> json, String currencyCode) {
    currencyModel = json[currencyCode] != null
        ? CurrencyModel.fromJson(json[currencyCode])
        : null;
  }

  Map<String, dynamic> toJson(String currencyCode) {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (currencyModel != null) {
      data[currencyCode] = currencyModel!.toJson();
    }
    return data;
  }
}
