import 'package:injectable/injectable.dart';
import 'package:insta_currency/core/apis/api_paths.dart';
import 'package:insta_currency/core/apis/api_service.dart';
import 'package:insta_currency/features/historical_rates/data/model/currency_rates.dart';

@Injectable()
class CurrencyRatesRepo {
  final ApiService apiService;

  CurrencyRatesRepo(this.apiService);

  Future<CurrencyRates> getRates(
      String startDate, String endDate, String symbols) async {
    var response = await apiService.getApi(
      ApiPaths.fluctuation,
      queryParameters: {
        "start_date": startDate,
        "end_date": endDate,
        "symbols": symbols
      },
    );
    return CurrencyRates.fromJson(response.data, symbols);
  }
}
