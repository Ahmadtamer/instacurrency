import 'package:flutter/foundation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:insta_currency/core/helper/app_date_formater.dart';
import 'package:insta_currency/features/historical_rates/data/model/currency_rates.dart';
import 'package:insta_currency/features/historical_rates/domain/repo/currency_rates_repo.dart';
import 'package:insta_currency/features/historical_rates/presenter/bloc/get_currency_rates_bloc/convert_currency_state.dart';
import 'package:rxdart/streams.dart';
import 'package:intl/intl.dart';

@Injectable()
class GetCurrencyRatesCubit extends Cubit<GetCurrencyRatesState> {
  final CurrencyRatesRepo _currencyRatesRepo;
  List<CurrencyRates> currencyRates = [];

  GetCurrencyRatesCubit(this._currencyRatesRepo)
      : super(GetCurrencyRatesInitial());

  Future<void> getRates(String symbols) async {
    try {
      currencyRates = [];
      List<DateTime> dates = AppDateFormatter.getWeekForNow();
      List<Stream> streams = [];
      for (int i = 0; i < dates.length - 1; i++) {
        String startDate = DateFormat("yyyy-MM-dd").format(dates[i]);
        String endDate = DateFormat("yyyy-MM-dd").format(dates[i + 1]);
        streams.add(Stream.fromFuture(
            _currencyRatesRepo.getRates(startDate, endDate, symbols)));
      }

      ConcatStream(streams).listen(
        (value) {
          if (value is CurrencyRates) {
            if (kDebugMode) {
              print(value.rates?.currencyModel?.endRate);
            }
            currencyRates.add(value);
            emit(GetCurrencyRatesSuccess(currencyRates));
          }
        },
      );
    } catch (e) {
      if (kDebugMode) {
        print(e.toString());
      }
      emit(GetCurrencyRatesFailed(e.toString()));
    }
  }
}
