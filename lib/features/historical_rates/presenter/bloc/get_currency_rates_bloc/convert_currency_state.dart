import 'package:insta_currency/features/historical_rates/data/model/currency_rates.dart';

abstract class GetCurrencyRatesState {}

class GetCurrencyRatesInitial extends GetCurrencyRatesState {}

class GetCurrencyRatesSuccess extends GetCurrencyRatesState {
  final List<CurrencyRates> currencyRates;

  GetCurrencyRatesSuccess(this.currencyRates);
}

class GetCurrencyRatesFailed extends GetCurrencyRatesState {
  final String error;

  GetCurrencyRatesFailed(this.error);
}
