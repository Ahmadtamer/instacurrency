import 'package:currency_picker/currency_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:insta_currency/core/constants/app_palette.dart';
import 'package:insta_currency/core/constants/app_style.dart';
import 'package:insta_currency/core/dependencies/dependency_init.dart';
import 'package:insta_currency/core/widgets/loading.dart';
import 'package:insta_currency/core/widgets/text_display.dart';
import 'package:insta_currency/features/historical_rates/presenter/bloc/get_currency_rates_bloc/convert_currency_bloc.dart';
import 'package:insta_currency/features/historical_rates/presenter/bloc/get_currency_rates_bloc/convert_currency_state.dart';
import 'package:insta_currency/features/historical_rates/presenter/widget/rates_table_widget.dart';

class HistoricalRatesScreen extends StatefulWidget {
  const HistoricalRatesScreen({Key? key}) : super(key: key);

  @override
  State<HistoricalRatesScreen> createState() => _HistoricalRatesScreenState();
}

class _HistoricalRatesScreenState extends State<HistoricalRatesScreen> {
  final GetCurrencyRatesCubit _currencyRatesCubit =
      getIt<GetCurrencyRatesCubit>();
  Currency? sourceCurrency;
  final CurrencyService _currencyService = CurrencyService();

  @override
  void initState() {
    super.initState();
    List<Currency> currencies = _currencyService.getAll();
    if (currencies.isNotEmpty) {
      sourceCurrency = currencies.first;
      fetchData();
    }
  }

  fetchData() {
    _currencyRatesCubit.getRates(sourceCurrency!.code);
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(16.h),
      child: buildHistoricalData(),
    );
  }

  Widget buildHistoricalData() {
    if (sourceCurrency == null) return Container();
    return BlocBuilder(
      bloc: _currencyRatesCubit,
      builder: (BuildContext context, GetCurrencyRatesState state) {
        if (state is GetCurrencyRatesSuccess) {
          return buildRatesWidget(state);
        } else {
          return const LoadingWidget();
        }
      },
    );
  }

  Widget buildRatesWidget(GetCurrencyRatesSuccess state) {
    return Container(
      padding: const EdgeInsets.all(16),
      decoration: AppStyle.roundedContainer,
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Row(children: [buildCurrencyCode(), buildArrowIcon()]),
            4.verticalSpace,
            buildCurrencyName(),
            16.verticalSpace,
            CurrencyRatesWidget(
              currencyRates: state.currencyRates,
              currency: sourceCurrency!,
            ),
          ]),
    );
  }

  Widget buildArrowIcon() {
    return const Icon(
      Icons.keyboard_arrow_down,
      color: AppPalette.primaryColor,
    );
  }

  Widget buildCurrencyName() {
    return AppTextDisplay(
      text: sourceCurrency!.name,
      fontSize: 18,
      textAlign: TextAlign.start,
    );
  }

  Widget buildCurrencyCode() {
    return InkWell(
      onTap: () => onCurrencyClicked(),
      child: AppTextDisplay(
        text: sourceCurrency!.code,
        fontSize: 24,
        fontWeight: FontWeight.w600,
        textAlign: TextAlign.start,
      ),
    );
  }

  onCurrencyClicked() {
    showCurrencyPicker(
      context: context,
      showFlag: true,
      showSearchField: true,
      showCurrencyName: true,
      showCurrencyCode: true,
      onSelect: (Currency currency) {
        setState(() => sourceCurrency = currency);
        fetchData();
      },
    );
  }
}
