import 'package:currency_picker/currency_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:insta_currency/core/constants/app_strings.dart';
import 'package:insta_currency/core/constants/app_style.dart';

import 'package:insta_currency/core/helper/double_extension.dart';
import 'package:insta_currency/core/widgets/text_display.dart';
import 'package:insta_currency/features/historical_rates/data/model/currency_rates.dart';

class CurrencyRatesWidget extends StatefulWidget {
  final List<CurrencyRates> currencyRates;
  final Currency currency;

  const CurrencyRatesWidget(
      {Key? key, required this.currencyRates, required this.currency})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => CurrencyRatesWidgetState();
}

class CurrencyRatesWidgetState extends State<CurrencyRatesWidget> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      child: buildRates(),
    );
  }

  Widget buildRates() {
    return DataTable(
      columnSpacing: 16.w,
      columns: [
        DataColumn(label: AppTextDisplay(translation: AppStrings.from)),
        DataColumn(label: AppTextDisplay(text: AppStrings.to)),
        DataColumn(label: AppTextDisplay(text: AppStrings.changes)),
        DataColumn(label: AppTextDisplay(text: AppStrings.dates)),
      ],
      rows: [
        for (int i = 0; i < widget.currencyRates.length; i++)
          DataRow(cells: [
            buildStartDataCell(i),
            buildEndDataCell(i),
            buildChangesCell(i),
            buildDateCell(i),
          ]),
      ],
    );
  }

  DataCell buildDateCell(int i) {
    return DataCell(
        AppTextDisplay(text: widget.currencyRates[i].endDate ?? ""));
  }

  DataCell buildChangesCell(int i) {
    return DataCell(AppTextDisplay(
        text: widget.currencyRates[i].rates?.currencyModel?.change
                ?.toPrecision(3)
                .toString() ??
            ""));
  }

  DataCell buildEndDataCell(int i) {
    return DataCell(AppTextDisplay(
        text: widget.currencyRates[i].rates?.currencyModel?.endRate
                ?.toPrecision(3)
                .toString() ??
            ""));
  }

  DataCell buildStartDataCell(int i) {
    return DataCell(AppTextDisplay(
        text: widget.currencyRates[i].rates?.currencyModel?.startRate
                ?.toPrecision(3)
                .toString() ??
            ""));
  }
}
