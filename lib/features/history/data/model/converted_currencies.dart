class ConvertedCurrencies {
  String? fromCurrency;
  String? toCurrency;
  double? amount;
  double? result;

  ConvertedCurrencies({
    this.fromCurrency,
    this.toCurrency,
    this.amount,
    this.result,
  });

  ConvertedCurrencies.fromJson(Map<dynamic, dynamic> json) {
    fromCurrency = json['fromCurrency'];
    toCurrency = json['toCurrency'];
    amount = json['amount'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (fromCurrency != null) data['fromCurrency'] = fromCurrency;
    if (toCurrency != null) data['toCurrency'] = toCurrency;
    if (amount != null) data['amount'] = amount;
    if (result != null) data['result'] = result;
    return data;
  }
}
