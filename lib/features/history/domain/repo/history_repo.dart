import 'package:flutter/foundation.dart';
import 'package:injectable/injectable.dart';
import 'package:insta_currency/core/helper/database_hepler.dart';
import 'package:insta_currency/features/history/data/model/converted_currencies.dart';

@Injectable()
class HistoryRepo {
  final DatabaseHelper db;

  HistoryRepo(this.db);

  Future<List<ConvertedCurrencies>> getAllHistory() async {
    List<ConvertedCurrencies> list = [];
    try {
      list = await db.getAllConvertedCurrencies();
    } catch (e) {
      if (kDebugMode) {
        print("Error on getAllHistory $e");
      }
    }
    return list;
  }
}
