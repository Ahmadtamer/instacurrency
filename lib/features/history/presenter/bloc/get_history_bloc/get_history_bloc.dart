import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:insta_currency/features/history/data/model/converted_currencies.dart';
import 'package:insta_currency/features/history/domain/repo/history_repo.dart';
import 'package:insta_currency/features/history/presenter/bloc/get_history_bloc/get_history_state.dart';

@Injectable()
class GetHistoryCubit extends Cubit<GetHistoryState> {
  final HistoryRepo getHistoryRepo;

  GetHistoryCubit(this.getHistoryRepo) : super(GetHistoryInitial());

  Future<void> getAllHistory() async {
    List<ConvertedCurrencies> list = await getHistoryRepo.getAllHistory();
    emit(GetHistorySuccess(list));
  }
}
