import 'package:insta_currency/features/history/data/model/converted_currencies.dart';

abstract class GetHistoryState {}

class GetHistoryInitial extends GetHistoryState {}

class GetHistorySuccess extends GetHistoryState {
  final List<ConvertedCurrencies> historyList;

  GetHistorySuccess(this.historyList);
}

class GetHistoryFailed extends GetHistoryState {
  final String error;

  GetHistoryFailed(this.error);
}
