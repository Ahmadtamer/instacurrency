import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:insta_currency/core/dependencies/dependency_init.dart';
import 'package:insta_currency/core/widgets/loading.dart';
import 'package:insta_currency/features/history/presenter/bloc/get_history_bloc/get_history_bloc.dart';
import 'package:insta_currency/features/history/presenter/bloc/get_history_bloc/get_history_state.dart';
import 'package:insta_currency/features/history/presenter/widget/history_item.dart';

class HistoryScreen extends StatefulWidget {
  const HistoryScreen({Key? key}) : super(key: key);

  @override
  State<HistoryScreen> createState() => _HistoryScreenState();
}

class _HistoryScreenState extends State<HistoryScreen> {
  final GetHistoryCubit _getHistoryCubit = getIt<GetHistoryCubit>();

  @override
  void initState() {
    super.initState();
    _getHistoryCubit.getAllHistory();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder(
          bloc: _getHistoryCubit,
          builder: (context, state) {
            if (state is GetHistorySuccess) {
              return ListView.builder(
                shrinkWrap: true,
                itemCount: state.historyList.length,
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                itemBuilder: (ctx, index) {
                  return index >= state.historyList.length
                      ? const LoadingWidget()
                      : Padding(
                          padding: const EdgeInsets.symmetric(vertical: 8.0),
                          child: HistoryItemWidget(
                              convertedCurrencies: state.historyList[index]),
                        );
                },
              );
            } else {
              return const LoadingWidget();
            }
          }),
    );
  }
}
