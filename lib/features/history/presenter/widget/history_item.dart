import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:insta_currency/core/constants/app_palette.dart';
import 'package:insta_currency/core/constants/app_style.dart';
import 'package:insta_currency/core/widgets/text_display.dart';
import 'package:insta_currency/features/history/data/model/converted_currencies.dart';

class HistoryItemWidget extends StatelessWidget {
  final ConvertedCurrencies convertedCurrencies;

  const HistoryItemWidget({Key? key, required this.convertedCurrencies})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 70.h,
      margin: AppStyle.largeHorizontalPadding,
      decoration: AppStyle.littleRoundedContainer,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: AppTextDisplay(
                text:
                    "${convertedCurrencies.amount} ${convertedCurrencies.fromCurrency}"),
          ),
          Container(
            padding: const EdgeInsets.all(8),
            decoration: const BoxDecoration(
                shape: BoxShape.circle, color: AppPalette.primaryColor),
            child: const Icon(
              Icons.double_arrow,
              color: AppPalette.white,
              size: 18,
            ),
          ),
          Expanded(
            child: AppTextDisplay(
                text:
                    "${convertedCurrencies.result} ${convertedCurrencies.toCurrency} "),
          ),
        ],
      ),
    );
  }
}
