import 'package:insta_currency/core/constants/app_strings.dart';

enum AppLanguages { english, arabic }

extension AppLanguageHelper on AppLanguages {
  String get value {
    switch (this) {
      case AppLanguages.english:
        return 'en';
      case AppLanguages.arabic:
        return 'ar';
    }
  }

  String get name {
    switch (this) {
      case AppLanguages.arabic:
        return AppStrings.arabic;
      case AppLanguages.english:
        return AppStrings.english;
      default:
        return AppStrings.english;
    }
  }
}

extension LanguageHelper on String {
  String getLanguageName() {
    switch (this) {
      case 'ar':
        return AppStrings.arabic;
      case 'en':
        return AppStrings.english;
      default:
        return AppStrings.english;
    }
  }
}
