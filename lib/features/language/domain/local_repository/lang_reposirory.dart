import 'package:injectable/injectable.dart';
import 'package:shared_preferences/shared_preferences.dart';

@Injectable()
class LanguageRepository {
  final String language = 'Language';

  Future<void> saveLang(String lang) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString(language, lang);
  }

  Future<String?> hasLang() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(language);
  }
}
