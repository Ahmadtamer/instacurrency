import 'dart:async';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:insta_currency/features/language/domain/local_repository/lang_reposirory.dart';
import 'lang_state.dart';

@Injectable()
class LanguageCubit extends Cubit<LanguageState> {
  static String lang = 'en';

  static const String arabic = 'ar';
  static const String english = 'en';
  final LanguageRepository langRepository;

  LanguageCubit(this.langRepository) : super(LanguageInitial());

  Future<void> getLanguage() async {
    // if (event is AppStart) {
    emit(LanguageLoading());
    String? result = await langRepository.hasLang();
    lang = result ?? english;
    emit(LanguageChanged(language: result ?? english));
  }

  Future<void> changeLanguage(String newLang) async {
    emit(LanguageLoading());
    lang = newLang;
    await langRepository.saveLang(newLang);
    emit(LanguageChanged(language: newLang));
  }
}
