import 'package:equatable/equatable.dart';

abstract class LanguageState extends Equatable {
  const LanguageState();

  @override
  List get props => [];
}

class LanguageInitial extends LanguageState {
  @override
  String toString() => 'LanguageInitial';
}

class LanguageLoading extends LanguageState {}

class LanguageChanged extends LanguageState {
  final String language;

  const LanguageChanged({required this.language});

  @override
  List get props => [language];
}
