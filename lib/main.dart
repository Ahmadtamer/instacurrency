import 'dart:async';

import 'package:flutter/material.dart';
import 'package:insta_currency/core/dependencies/dependency_init.dart';

import 'app.dart';
import 'core/bloc_provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  configureDependencies();

  runZonedGuarded(() async {
    runApp(AppMainBlocProvider(child: const MyApp()));
  }, (_, __) {});
}
